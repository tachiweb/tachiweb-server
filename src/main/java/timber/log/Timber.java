package timber.log;

/**
 * Project: TachiServer
 * Author: nulldev
 * Creation Date: 12/07/16
 */

/**
 * Compat class
 */
public class Timber {
    public static void e(String string) {
        System.out.println("TIMBER: " + string);
    }
}
